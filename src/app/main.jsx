import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import NavBar from './components/nav_bar';

class Main extends Component {
  render() {
    return <div>
      <NavBar />
      <p id="start">Not long ago in a house not far from central London&hellip;</p>
      <h1>STAR WARS<sub>A Wiki Page</sub></h1>
      <div id="titles">
        <div id="titlecontent">
          <p className="center"><br />
            A NEW HOPE FOR CHARILAOS</p>
          <p>Technology has gone crazy, software is used everywhere. London is a tough market. There is a lot to learn in order to succeed.</p>
          <p>Charilaos, a young man from Athens who ended up in UK likes challenges. He decided to start learning.</p>
          <p>Then, ReactJS came on his way. At first there were many conflicts but a few weeks later things started calming down</p>
          <p>Soon enough peace was announced between both ends. Charilaos then realised that this alliance will be benefitial.</p>
          <p>The quest for the next big adventure has begun...</p>
          <p>People from the Star Wars movies can be viewed <a href="/people/1">here</a></p>
        </div>
      </div>
      <div id="footer">
        Created By <a target="_blank" href="https://www.linkedin.com/in/charilaos-georgakakis/">Charilaos Georgakakis</a>
      </div>
    </div>
  }
}

module.exports = Main
