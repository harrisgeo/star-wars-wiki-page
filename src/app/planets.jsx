import React from 'react';
import * as Promise from 'bluebird';
import NavBar from './components/nav_bar';
import API from './components/api';
import Load from './components/load';
import * as BS from 'react-bootstrap';

  
export default class Person extends React.Component {

  constructor(props) {
    super(props);
    var state = {
      planets: API.request('planets/'+this.props.params.id),
    }


    state = Promise.all(_.map(state)).then((data) => {
      return {
        planets: data[0],
      }
    });
    this.state = { promise: state }
  }

  renderPage(res) {
    console.log('res',res.planets)
    var {planets} = res
    var i = 0;

    return <div>
      <NavBar />
      <BS.Table bordered>
        <tbody>
          <tr><th>name</th><td>{planets.name}</td></tr>
          <tr><th>population</th><td>{planets.population}</td></tr>
          <tr><th>terrain</th><td>{planets.terrain}</td></tr>
          <tr><th>climate</th><td>{planets.climate}</td></tr>
          <tr><th>surface_water</th><td>{planets.surface_water}</td></tr>
          <tr><th>diameter</th><td>{planets.diameter}</td></tr>
          <tr><th>gravity</th><td>{planets.gravity}</td></tr>
          <tr><th>rotation_period</th><td>{planets.rotation_period}</td></tr>
          <tr><th>orbital_period</th><td>{planets.orbital_period}</td></tr>
        </tbody>
      </BS.Table>
    </div>
  }

  render() {
    return <Load promise={this.state.promise} render={this.renderPage} />
  }
}
