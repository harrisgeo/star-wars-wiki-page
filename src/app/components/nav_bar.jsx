import React from 'react';
import * as BS from 'react-bootstrap';

  
export default class NavBar extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return <nav className="navbar navbar-inverse navbar-fixed-top" id="wiki-navbar">
      <div className="container">
        <div className="navbar-header">
          <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span className="sr-only">Toggle navigation</span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>
          <a className="navbar-brand" id="yo" href="/">Star Wars Wiki</a>
        </div>
        <div id="navbar" className="collapse navbar-collapse">
          <ul className="nav navbar-nav">
            <li><a href="/people/1">People</a></li>
          </ul>
        </div>
      </div>
    </nav>
  }
}
