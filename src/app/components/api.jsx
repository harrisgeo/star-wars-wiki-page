var request = require('superagent');
var Promise = require('bluebird');
var _ = require('lodash');

export default {

  request(section = '', page = 0) {
    return new Promise((resolve, reject) => {
      var url = 'http://swapi.co/api/' + section
      
      if (page)
        url += '/?page='+page

      var req = request.get(url);
      req.end((err, res) => {
        if (res.status == 200)
          resolve(res.body)
        else {
          // console.log('rej')
          reject(res.body)
        }
      });
    });
  }
}
