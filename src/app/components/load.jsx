var React = require('react');

var {Panel, Col} = require('react-bootstrap');

var APILoad = React.createClass({

  getInitialState: function () {
    return {
      loaded: false,
      error: null,
      data: null  
    };
  },

  getDefaultProps: function () {
    return {
      loadingText: 'Loading...'
    }
  },

  propTypes: {
    children: React.PropTypes.element,
    render: React.PropTypes.func
  },

  childContextTypes: {
    data: React.PropTypes.any
  },

  getChildContext: function () {
    return {
      data: this.props.promise.isFulfilled() ? this.props.promise.value() : null
    }
  },

  contextTypes: {
    api: React.PropTypes.object,
    alerts: React.PropTypes.object
  },

  componentWillMount: function () {
    this.componentWillReceiveProps(this.props);
  },

  componentWillReceiveProps: function (props) {
    if (!props.promise || !props.promise.then) {
      console.error('APILoad component requires a promise')
      return null;
    }

    props.promise.finally(() => { this.forceUpdate() });
  },

  render: function () {
    if (this.props.promise.isRejected()) {
      return <h2 id="error">404 This is not page you are looking for</h2>
    }
    if (this.props.promise.isFulfilled()) {
      if (this.props.render) {
        return this.props.render(this.props.promise.value());
      }

      if (this.props.children) {
        var obj = {}
        if (this.props.target) {
          obj[this.props.target] = this.props.promise.value();
        }
        return React.cloneElement(this.props.children, obj)
      }

      console.error('APILoad requires either children or a render function')
    }
   
    var props = {
      className: 'apiloader-loading',
      style: {
        padding: '50px',
        textAlign: 'center',
        fontSize: '150%',
        color: '#666'
      }
    }
    return <div {...props}>{this.props.loadingText}</div>;
  }

});

module.exports = APILoad;
