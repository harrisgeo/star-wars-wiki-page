import React from 'react';
import * as Promise from 'bluebird';
import API from './components/api';
import Load from './components/load';
import * as BS from 'react-bootstrap';
import NavBar from './components/nav_bar';
  
export default class Person extends React.Component {

  constructor(props) {
    super(props);
    var state = {
      starships: API.request('starships/'+this.props.params.id),
    }

    state = Promise.all(_.map(state)).then((data) => {
      return {
        starships: data[0],
      }
    });
    this.state = { promise: state }
  }

  renderPage(res) {
    console.log('res',res.starships)
    var {starships} = res
    var i = 0;

    return <div>
      <NavBar />
      <BS.Table bordered>
        <tbody>
          <tr><th>Name</th><td>{starships.name}</td></tr>
          <tr><th>model</th><td>{starships.model}</td></tr>
          <tr><th>crew</th><td>{starships.crew}</td></tr>
          <tr><th>starship_class</th><td>{starships.starship_class}</td></tr>
          <tr><th>passengers</th><td>{starships.passengers}</td></tr>
          <tr><th>cargo_capacity</th><td>{starships.cargo_capacity}</td></tr>
          <tr><th>consumables</th><td>{starships.consumables}</td></tr>
          <tr><th>hyperdrive_rating</th><td>{starships.hyperdrive_rating}</td></tr>
          <tr><th>length</th><td>{starships.length}</td></tr>
          <tr><th>manufacturer</th><td>{starships.manufacturer}</td></tr>
        </tbody>
      </BS.Table>
    </div>
  }

  render() {
    return <Load promise={this.state.promise} render={this.renderPage} />
  }
}
