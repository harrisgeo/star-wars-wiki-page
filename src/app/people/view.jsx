import React from 'react';
import * as Promise from 'bluebird';
import API from '../components/api';
import Load from '../components/load';
import Homeworld from './subcomponents/homeworld';
import Starship from './subcomponents/starship';
import NavBar from '../components/nav_bar';
import * as BS from 'react-bootstrap';
  
export default class Person extends React.Component {

  constructor(props) {
    super(props);
    var state = {
      person: API.request('people/'+this.props.params.id),
      planets: API.request('planets'),
      starships: API.request('starships'),
    }

    state = Promise.all(_.map(state)).then((data) => {
      var person = data[0]
      var planetId = person.homeworld.replace('http://swapi.co/api/planets/','').slice(0,-1)
      return {
        person: person,
        planets: data[1],
        startships: data[2]    
      }
    });
    this.state = { promises: state }
  }

  renderPage(res) {
    console.log('res',res)
    var {person, planets, startships} = res
    var i = 0;
    var starshipId = ''
    return <div>
      <NavBar />
      <BS.Table bordered>
        <tbody>
          <tr>
            <th>Name</th>
            <td>{person.name}</td>
          </tr>
          <tr>
            <th>Gender</th><td>{person.gender}</td>
          </tr>
          <tr>
            <th>Birth Year</th>
            <td>{person.birth_year}</td>
          </tr>
          <tr>
            <th>Hair Color</th>
            <td>{person.hair_color}</td>
          </tr>
          <tr>
            <th>Home</th>
            <td>
              <Homeworld planetId={person.homeworld.replace('http://swapi.co/api/planets/','').slice(0,-1)} />
            </td>
          </tr>
          {_.map(person.starships, (obj) => {
            i += 1
            starshipId = obj.replace('http://swapi.co/api/starships/','').slice(0, -1)
            return <tr key={starshipId}>
              <th>
              Starship {i}  
              </th>
              <td>
                <Starship starshipId={starshipId} />
              </td>
            </tr>
          })}
        </tbody>
      </BS.Table>
    </div>
  }

  render() {
    return <Load promise={this.state.promises} render={this.renderPage} />
  }
}
