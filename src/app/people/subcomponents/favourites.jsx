import React from 'react';
import * as BS from 'react-bootstrap';

export default class Favourites extends React.Component {

  constructor(props){
    super(props);
    this.removeFavourite = this.removeFavourite.bind(this)
    this.cleanFavourites = this.cleanFavourites.bind(this)
    this.favouritesActions = this.favouritesActions.bind(this)
  }
 
  removeFavourite(id) {
    var latest = localStorage.length
    localStorage.removeItem(latest - 1)
    this.forceUpdate()
  }

  cleanFavourites() {
    localStorage.clear()
    this.forceUpdate()
  }

  favouritesActions() {
    if (localStorage.length) {
      return <div>
        <BS.Button bsSize="xsmall" bsStyle="danger" onClick={() => this.removeFavourite()}>Remove <BS.Glyphicon glyph="minus" /></BS.Button>
        <br/>
        <br/>
        <BS.Button bsSize="xsmall" bsStyle="danger" onClick={() => this.cleanFavourites()}>Clear Favourites <BS.Glyphicon glyph="trash" /></BS.Button>
      </div>
    }
    else {
      return <div>No Favourites yet...</div>
    }
  }

  render() {
    var storage = this.props.storage
    var explode,id,name
    var i = 0

    return <div id="favourites">
      <h2>Favourites</h2>
      {_.map(storage, (item) => {
        i += 1
        if (item) {
          explode = _.split(item, "&")
          id = parseInt(explode[0])
          name = explode[1]
          return <p key={i}>
            <a href={"/people/view/"+id}>{name}</a>
          </p>
        }
        else
          return <div key={i}></div>
      })}
      <div id="remove-fav">
        {this.favouritesActions()}
      </div>
    </div>
  }
}
