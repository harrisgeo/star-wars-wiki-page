import React from 'react';
import * as Promise from 'bluebird';
import API from '../../components/api';
import Load from '../../components/load';
  
export default class Person extends React.Component {

  constructor(props) {
    super(props);
    var id = this.props.planetId
    var state = {
      planet: API.request('planets/' + id),
    }

    state = Promise.all(_.map(state)).then((data) => {
      return {
        planet: data[0],
      }
    });
    this.state = { promises: state, id:id }
    this.renderPage = this.renderPage.bind(this);
  }

  renderPage(res) {
    var i = 0;
    return <div>
      <a href={'/planets/view/'+this.state.id}>{res.planet.name}</a>
    </div>
  }

  render() {
    return <Load promise={this.state.promises} render={this.renderPage} />
  }
}
