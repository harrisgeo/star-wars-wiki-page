import React from 'react';
import * as Promise from 'bluebird';
import API from '../../components/api';
import Load from '../../components/load';
  
export default class StarShipComponent extends React.Component {

  constructor(props) {
    super(props);
    var id = this.props.starshipId
    var state = {
      starship: API.request('starships/' + id),
    }

    state = Promise.all(_.map(state)).then((data) => {
      return {
        starship: data[0],
      }
    });
    this.state = { promises: state, id:id }
    this.renderPage = this.renderPage.bind(this);
  }

  renderPage(res) {
    var i = 0;
    return <div>
      <a href={'/starships/view/'+this.state.id}>{res.starship.name}</a>
    </div>
  }

  render() {
    return <Load promise={this.state.promises} render={this.renderPage} />
  }
}
