import React from 'react';
import * as Promise from 'bluebird';
import API from '../components/api';
import Load from '../components/load';
import * as BS from 'react-bootstrap';
import Favourites from './subcomponents/favourites';
import NavBar from '../components/nav_bar';

export default class People extends React.Component {

  constructor(props){
    super(props);
    this.state = { 
      people: API.request('people', this.props.params.page), 
    }
    this.addToFavourites = this.addToFavourites.bind(this)
    this.renderPage = this.renderPage.bind(this)

  }

  addToFavourites(name, id) {
    localStorage.setItem(localStorage.length, id+'&'+name)
    this.forceUpdate()
  }

  pagination(res) {
    var id = parseInt(this.props.params.page)
    var prev = <div></div>
    var next = <div></div>
    if (res.previous)
      prev = <BS.Button bsSize="xsmall" href={"/people/"+(id-1)}><BS.Glyphicon glyph="arrow-left" /></BS.Button>
    if (res.next)
      next = <BS.Button bsSize="xsmall" href={"/people/"+(id+1)}><BS.Glyphicon glyph="arrow-right" /></BS.Button>

    return <div id="pagination">{prev} {next}</div>
  }

  renderPage(res) {
    var that = this
    return <div>
      <NavBar />
      <div id="people">
        <h2>People</h2>
        <table id="people-table">
          <tbody>
            <tr>
              <th>id</th>
              <th>Name</th>
            </tr>
            {
              _.map(res.results, (res) => {
                var id = res.url.replace('http://swapi.co/api/people/','').slice(0,-1)
                return <tr key={res.name}>
                  <td>{id}</td>
                  <td>
                    <a href={"/people/view/"+id}>{res.name}</a>
                    <BS.Button id="res" onClick={() => that.addToFavourites(res.name, id)} bsStyle="link"><BS.Glyphicon className="fav-glyph" glyph="star" /></BS.Button>
                  </td>
                </tr>
              })
            }
          </tbody>
        </table>
        {this.pagination(res)}
      </div>
      <div>
        <Favourites storage={localStorage}/>
      </div>
    </div>
  }

  render() {
    return <div>
      <Load promise={this.state.people} render={this.renderPage} />
    </div>
  }
}
