import {Router, Route, browserHistory} from 'react-router';
import MainComponent from './main';
import People from './people';
import StarshipView from './starships';
import PeopleView from './people/view';
import PlanetsView from './planets';
import React, {Component} from 'react';

export default class extends Component {
  render() {
    return <Router history={browserHistory}>
      <Route path="/" component={MainComponent}></Route>
      <Route path="/people/:page" component={People}/>
      <Route path="/people/view/:id" component={PeopleView}/>
      <Route path="/starships/view/:id" component={StarshipView}/>
      <Route path="/planets/view/:id" component={PlanetsView}/>
    </Router>
  }
}
